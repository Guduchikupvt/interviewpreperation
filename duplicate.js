
//Find duplicate Element brute force method means compare each value with the rest of the value

function findDuplicate (numberArray){
  const duplicateResult = [];
  //If you are not using for loop
  numberArray.forEach((list, index)=>{
    //In for loop we have to create J for second loop so this below line is an alternative
    numberArray.slice(index+1,numberArray.length).forEach((list2)=>{
      //Checking for if both the values are same or not & if you already encounter the value before
      if(list === list2 && !duplicateResult.includes(list)){
        duplicateResult.push(list);
      }
    })
  });
  return duplicateResult;
}

function findDuplicateWithRepetationTimes (numberArray){
  const result = {};
  numberArray.forEach((list, index)=> {
    !Object.keys(result).includes(list.toString()) && numberArray.slice(index+1,numberArray.length).forEach((list2)=>{
      if(list === list2){
        console.log(Object.keys(result));
        if(Object.keys(result).includes(list2.toString())){
          result[list2] = ++result[list2];
        } else {
          result[list2] = 2;
        }
      }
    })
  });
  return result;
}
//Find duplicate Element using hashmap
//Hash map in javascript has below methods :
// get(key), set(key, value), has(key), delete(key), clear

function findDuplicateUsingHashMap (numberArray){
  const duplicateResult = new Map();
  const result = [];
  numberArray.forEach((list)=>{
    if(duplicateResult.get(list)){
      duplicateResult.set(list, duplicateResult.get(list)+ 1);
    } else {
      duplicateResult.set(list, 1);
    }
  });

  duplicateResult.forEach((list, key)=>{
    if(list >= 2){
      result.push(key);
    }
    
  });
  return result;
}

function findDuplicateWithCountUsingHashMap (numberArray){
  const duplicateResult = new Map();
  const result = [];
  numberArray.forEach((list)=>{
    if(duplicateResult.get(list)){
      duplicateResult.set(list, duplicateResult.get(list)+ 1);
    } else {
      duplicateResult.set(list, 1);
    }
  });

  duplicateResult.forEach((list, key)=>{
    if(list >= 2){
      result.push({'duplicateValue': key, 'repeatationTime': list});
    }
    
  });
  return result;
}

findDuplicateWithCountUsingHashMap([1,3,2,12,23,1,2,3,1,22,23,9,8,5,7,5,9,5,1,5,23]);
